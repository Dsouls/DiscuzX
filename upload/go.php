<?php

function checkSteam($url) {
    if (strpos(trim($url), 'steam://') === 0) {
        return trim($url);
    }
    else {
        return '/';
    }
}

$url = isset($_GET['url']) ? checkSteam($_GET['url']) : '/';
header('location: '.$url);