<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: api.php 33591 2013-07-12 06:39:49Z andyzheng $
 */

define('IN_API', true);
define('CURSCRIPT', 'api');

$mod = !empty($_GET['mod']) ? $_GET['mod'] : '';
if(empty($mod)) {
    exit('Access Denied');
}

global $_G;
require_once './source/class/class_core.php';

$discuz = C::app();
$discuz->init_cron = false;
$discuz->init_session = false;
$discuz->init();

switch ($_GET['mod']) {
    case 'group':
        echo json_encode($_G['group']);
        break;
    case 'profilegroup':
        echo json_encode($_G['setting']['profilegroup']);
        break;
    default:
        echo 'HAHA';
        break;
}
