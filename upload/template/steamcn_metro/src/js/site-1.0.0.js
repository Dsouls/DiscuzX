﻿define(["jquery", "vendor"], function($) {
    "use strict";

    var site = {
        common: {
            setup: function() {
                /* Search Bar */
                var submitButton = $(".search-bar-form button[type=submit]");
                $(".search-bar-form .dropdown-menu a").click(function() {
                    $(".search-bar-form input[name='mod']").val($(this).data("mod"));
                    $(".search-bar-form .dropdown .btn span:first-child").text($(this).text());
                });

                $(".search-bar-form").submit(function() {
                    switch ($(this).find("input[name='mod']").val()) {
                    case "google":
                        window.open("https://www.google.com.hk/search?q=" + encodeURIComponent("site:keylol.com " + $(this).find(".search-box").val()));
                        break;

                    case "baidu":
                        window.open("https://www.baidu.com/s?wd=" + encodeURIComponent("site:keylol.com " + $(this).find(".search-box").val()));
                        break;

                    default:
                        return true;
                    }
                    return false;
                });

                $(".search-bar-form").on("touchstart mouseenter", function() {
                    submitButton.addClass("search-button-visible");
                }).on("mouseleave", function() {
                    submitButton.removeClass("search-button-visible");
                });
            }
        },
        portal: {
            setupIndex: function() {
                /* Froum Threads */
                (function() {
                    function activeTab(tab, tabs, container) {
                        tabs.removeClass("active");
                        tab.addClass("active");
                        container.html(tab.children().slice(1).clone());
                    }

                    $(".tabbed-list").each(function() {
                        var container = $("<div class='portal-articles'></div>");
                        $(this).append(container);
                        var tabs = $("> section", this);
                        tabs.hover(function() {
                            if (!$(this).hasClass("active"))
                                activeTab($(this), tabs, container);
                        });
                        activeTab(tabs.first(), tabs, container); // Activate first tab
                    });
                })();

                /* Steam Server Status */
                $.getJSON("http://steamcn.sinaapp.com/steamstatus/")
                    .done(function(data) {
                        function statusText(service) {
                            if (service.status === "major") {
                                return "<i class=\"server-status-indicator server-status-indicator-down\"></i>" + service.title;
                            } else if (service.status === "minor") {
                                return "<i class=\"server-status-indicator server-status-indicator-delayed\"></i>" + service.title;
                            }
                            return service.title;
                        }

                        if (data.success) {
                            $("#server-status-online-users").html(statusText(data.services.online));
                            $("#server-status-store").html(statusText(data.services.store));
                            $("#server-status-community").html(statusText(data.services.community));
                            $("#server-status-webapi").html(statusText(data.services.webapi));
                            $("#server-status-cm-cn").html(statusText(data.services["cm-CN"]));
                            $("#server-status-cm-us").html(statusText(data.services["cm-US"]));
                            $("#server-status-cm-eu").html(statusText(data.services["cm-EU"]));
                            $("#server-status-dota2").html(statusText(data.services.dota2));
                            $("#server-status-csgo").html(statusText(data.services.csgo));
                        } else {
                            $("#server-status-module").html("<p id=\"server-status-failed-text\">获取失败</p>");
                        }
                    })
                    .fail(function() {
                        $("#server-status-module > section").html("<p id=\"server-status-failed-text\">获取失败</p>");
                    });

                /* 7L */
                $.getJSON("plugin.php?id=steamcn_gift:portal", function(data) {
                    if (data.success) {
                        var giveawayElement = $(".module-7l ul");
                        $(".module-7l .count span:first-child").text(data.count);
                        for (var i = 0; i < data.giveaway.length; ++i) {
                            var g = data.giveaway[i];
                            giveawayElement.append("<li><a style=\"background-image:url(" + g.img + ");\" target=\"_blank\" href=\"forum.php?mod=viewthread&tid=" + g.id + "\"><span class=\"giveaway-end\">" + g.end + "赠出</span></a></li>");
                        }
                    } else {
                        $(".module-7l > section").html("<p class=\"module-7l-failed-text\">获取失败</p>");
                    }
                });

                /* Flashing Clip News */
                $('#flashing-clip-news').nivoSlider({ controlNav: true });

                /* Featured Games */
                $("#featuregames").show().sliderTabs({
                    autoplay: 15000,
                    mousewheel: false,
                    tabArrows: false,
                    height: 354,
                    tabHeight: 24,
                    position: "bottom"
                });
            }
        }
    };

    var init = function() {
        site.common.setup();
        site.portal.setupIndex();
    }

    document.readyState === "interactive" ? init() : $(document).ready(init);

    return site;
});