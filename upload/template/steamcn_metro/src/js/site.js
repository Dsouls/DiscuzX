﻿(function () {
    "use strict";
    var site = {
        common: {
            setup: function () {
                /* Search Bar */
                var submitButton = jq(".search-bar-form button[type=submit]");
                jq(".search-bar-form .dropdown-menu a").click(function () {
                    var mod = jq(this).data("mod");
                    jq(".search-bar-form input[name='mod']").val(mod);
                    jq(".search-bar-form .dropdown .btn span:first-child").text(jq(this).text());
                    if (jq(this).hasClass("search-bar-remember")) {
                        saveUserdata("search_mod", mod);
                    }
                });
                var lastMod = loadUserdata("search_mod");
                if (lastMod) {
                    jq(".search-bar-remember[data-mod=" + lastMod + "]").click();
                }

                jq(".search-bar-form").submit(function () {
                    switch (jq(this).find("input[name='mod']").val()) {
                        case "google":
                            window.open("https://www.google.com/search?q=" + encodeURIComponent("site:keylol.com " + jq(this).find(".search-box").val()));
                            break;

                        case "baidu":
                            window.open("https://www.baidu.com/s?wd=" + encodeURIComponent("site:keylol.com " + jq(this).find(".search-box").val()));
                            break;

                        case "bing":
                            window.open("https://www.bing.com/search?q=" + encodeURIComponent("site:keylol.com " + jq(this).find(".search-box").val()));
                            break;

                        default:
                            return true;
                    }
                    return false;
                });

                jq(".search-bar-form").on("touchstart mouseenter", function () {
                    submitButton.addClass("search-button-visible");
                }).on("mouseleave", function () {
                    submitButton.removeClass("search-button-visible");
                });

                jq(".search-bar").show(); // show search-bar when ready
            }
        }
    };

    var init = function () {
        site.common.setup();
    };

    jq(document).ready(init);
})();
