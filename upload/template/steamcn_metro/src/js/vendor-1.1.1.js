﻿///#source 1 1 /template/steamcn_metro/src/js/jquery.nivo.slider.js
/*
 * jQuery Nivo Slider v3.2
 * http://nivo.dev7studios.com
 *
 * Copyright 2012, Dev7studios
 * Free to use and abuse under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 */

(function ($) {
    var NivoSlider = function (element, options) {
        // Defaults are below
        var settings = $.extend({}, $.fn.nivoSlider.defaults, options);

        // Useful variables. Play carefully.
        var vars = {
            currentSlide: 0,
            currentImage: '',
            totalSlides: 0,
            running: false,
            paused: false,
            stop: false,
            controlNavEl: false
        };

        // Get this slider
        var slider = $(element);
        var anchor = slider.wrap("<a target=\"_blank\" />").parent();
        anchor.wrap("<div />").parent().css("position", "relative");
        slider.data('nivo:vars', vars).addClass('nivoSlider');

        // Find our slider children
        var kids = slider.children();
        kids.each(function () {
            var child = $(this);
            var link = '';
            if (!child.is('img')) {
                if (child.is('a')) {
                    child.addClass('nivo-imageLink');
                    link = child;
                }
                child = child.find('img:first');
            }

            if (link !== '') {
                link.css('display', 'none');
            }
            child.css('display', 'none');
            vars.totalSlides++;
        });

        // If randomStart
        if (settings.randomStart) {
            settings.startSlide = Math.floor(Math.random() * vars.totalSlides);
        }

        // Set startSlide
        if (settings.startSlide > 0) {
            if (settings.startSlide >= vars.totalSlides) { settings.startSlide = vars.totalSlides - 1; }
            vars.currentSlide = settings.startSlide;
        }

        // Get initial image
        if ($(kids[vars.currentSlide]).is('img')) {
            vars.currentImage = $(kids[vars.currentSlide]);
        } else {
            vars.currentImage = $(kids[vars.currentSlide]).find('img:first');
        }

        // Show initial link
        if ($(kids[vars.currentSlide]).is('a')) {
            $(kids[vars.currentSlide]).css('display', 'block');
        }

        // Set first background
        var sliderImg = $('<img/>').addClass('nivo-main-image');
        sliderImg.attr('src', vars.currentImage.attr('src')).show();
        slider.append(sliderImg);

        // Detect Window Resize
        $(window).resize(function () {
            slider.children('img').width(slider.width());
            sliderImg.attr('src', vars.currentImage.attr('src'));
            $('.nivo-slice').remove();
            $('.nivo-box').remove();
        });

        //Create caption
        slider.append($('<div class="nivo-caption"></div>'));

        // Process caption function
        var processCaption = function (settings) {
            var nivoCaption = $('.nivo-caption', slider);
            if (vars.currentImage.attr('title') != '' && vars.currentImage.attr('title') != undefined) {
                var title = vars.currentImage.attr('title');
                if (title.substr(0, 1) == '#') title = $(title);

                var titleAnchor = title.children("a");
                if (titleAnchor != undefined)
                    anchor.attr("href", titleAnchor.attr("href"));

                if (nivoCaption.css('display') == 'block') {
                    setTimeout(function () {
                        nivoCaption.fadeOut(200, function () { nivoCaption.html(title.html()).fadeIn(200) });
                    }, settings.animSpeed);
                } else {
                    nivoCaption.html(title.html());
                    nivoCaption.stop().fadeIn(settings.animSpeed);
                }
            } else {
                nivoCaption.stop().fadeOut(settings.animSpeed);
            }
        }

        //Process initial  caption
        processCaption(settings);

        // In the words of Super Mario "let's a go!"
        var timer = 0;
        if (!settings.manualAdvance && kids.length > 1) {
            timer = setInterval(function () { nivoRun(slider, kids, settings, false); }, settings.pauseTime);
        }

        // Add Direction nav
        if (settings.directionNav) {
            var directionNav = $('<div class="nivo-directionNav"><a class="nivo-prevNav">' + settings.prevText + '</a><a class="nivo-nextNav">' + settings.nextText + '</a></div>');
            anchor.after(directionNav);

            $(directionNav).on('click', 'a.nivo-prevNav', function () {
                if (vars.running) { return false; }
                clearInterval(timer);
                timer = '';
                vars.currentSlide -= 2;
                nivoRun(slider, kids, settings, 'prev');
            });

            $(directionNav).on('click', 'a.nivo-nextNav', function () {
                if (vars.running) { return false; }
                clearInterval(timer);
                timer = '';
                nivoRun(slider, kids, settings, 'next');
            });
        }

        // Add Control nav
        if (settings.controlNav) {
            vars.controlNavEl = $(".nivo-controlNav");
            // 不自动添加
            // slider.after(vars.controlNavEl);
            // for(var i = 0; i < kids.length; i++){
            //     if(settings.controlNavThumbs){
            //         vars.controlNavEl.addClass('nivo-thumbs-enabled');
            //         var child = kids.eq(i);
            //         if(!child.is('img')){
            //             child = child.find('img:first');
            //         }
            //         if(child.attr('data-thumb')) vars.controlNavEl.append('<a class="nivo-control" rel="'+ i +'"><img src="'+ child.attr('data-thumb') +'" alt="" /></a>');
            //     } else {
            //         vars.controlNavEl.append('<a class="nivo-control" rel="'+ i +'">'+ (i + 1) +'</a>');
            //     }
            // }

            //Set initial active link
            $('li:eq(' + vars.currentSlide + ')', vars.controlNavEl).addClass('active');

            $('a', vars.controlNavEl).bind('click', function () {
                if (vars.running) return false;
                if ($(this).parent().hasClass('active')) return false;
                clearInterval(timer);
                timer = '';
                sliderImg.attr('src', vars.currentImage.attr('src'));
                vars.currentSlide = $(this).parent().index() - 1;
                nivoRun(slider, kids, settings, 'control');
            });
        }

        //For pauseOnHover setting
        if (settings.pauseOnHover) {
            slider.hover(function () {
                vars.paused = true;
                clearInterval(timer);
                timer = '';
            }, function () {
                vars.paused = false;
                // Restart the timer
                if (timer === '' && !settings.manualAdvance) {
                    timer = setInterval(function () { nivoRun(slider, kids, settings, false); }, settings.pauseTime);
                }
            });
        }

        // Event when Animation finishes
        slider.bind('nivo:animFinished', function () {
            sliderImg.attr('src', vars.currentImage.attr('src'));
            vars.running = false;
            // Hide child links
            $(kids).each(function () {
                if ($(this).is('a')) {
                    $(this).css('display', 'none');
                }
            });
            // Show current link
            if ($(kids[vars.currentSlide]).is('a')) {
                $(kids[vars.currentSlide]).css('display', 'block');
            }
            // Restart the timer
            if (timer === '' && !vars.paused && !settings.manualAdvance) {
                timer = setInterval(function () { nivoRun(slider, kids, settings, false); }, settings.pauseTime);
            }
            // Trigger the afterChange callback
            settings.afterChange.call(this);
        });

        // Add slices for slice animations
        var createSlices = function (slider, settings, vars) {
            if ($(vars.currentImage).parent().is('a')) $(vars.currentImage).parent().css('display', 'block');
            $('img[src="' + vars.currentImage.attr('src') + '"]', slider).not('.nivo-main-image,.nivo-control img').width(slider.width()).css('visibility', 'hidden').show();
            var sliceHeight = ($('img[src="' + vars.currentImage.attr('src') + '"]', slider).not('.nivo-main-image,.nivo-control img').parent().is('a')) ? $('img[src="' + vars.currentImage.attr('src') + '"]', slider).not('.nivo-main-image,.nivo-control img').parent().height() : $('img[src="' + vars.currentImage.attr('src') + '"]', slider).not('.nivo-main-image,.nivo-control img').height();

            for (var i = 0; i < settings.slices; i++) {
                var sliceWidth = Math.round(slider.width() / settings.slices);

                if (i === settings.slices - 1) {
                    slider.append(
                        $('<div class="nivo-slice" name="' + i + '"><img src="' + vars.currentImage.attr('src') + '" style="position:absolute; width:' + slider.width() + 'px; height:auto; display:block !important; top:0; left:-' + ((sliceWidth + (i * sliceWidth)) - sliceWidth) + 'px;" /></div>').css({
                            left: (sliceWidth * i) + 'px',
                            width: (slider.width() - (sliceWidth * i)) + 'px',
                            height: sliceHeight + 'px',
                            opacity: '0',
                            overflow: 'hidden'
                        })
                    );
                } else {
                    slider.append(
                        $('<div class="nivo-slice" name="' + i + '"><img src="' + vars.currentImage.attr('src') + '" style="position:absolute; width:' + slider.width() + 'px; height:auto; display:block !important; top:0; left:-' + ((sliceWidth + (i * sliceWidth)) - sliceWidth) + 'px;" /></div>').css({
                            left: (sliceWidth * i) + 'px',
                            width: sliceWidth + 'px',
                            height: sliceHeight + 'px',
                            opacity: '0',
                            overflow: 'hidden'
                        })
                    );
                }
            }

            $('.nivo-slice', slider).height(sliceHeight);
            //sliderImg.stop().animate({
            //    height: $(vars.currentImage).height()
            //}, settings.animSpeed);
        };

        // Add boxes for box animations
        var createBoxes = function (slider, settings, vars) {
            if ($(vars.currentImage).parent().is('a')) $(vars.currentImage).parent().css('display', 'block');
            $('img[src="' + vars.currentImage.attr('src') + '"]', slider).not('.nivo-main-image,.nivo-control img').width(slider.width()).css('visibility', 'hidden').show();
            var boxWidth = Math.round(slider.width() / settings.boxCols),
                boxHeight = Math.round($('img[src="' + vars.currentImage.attr('src') + '"]', slider).not('.nivo-main-image,.nivo-control img').height() / settings.boxRows);


            for (var rows = 0; rows < settings.boxRows; rows++) {
                for (var cols = 0; cols < settings.boxCols; cols++) {
                    if (cols === settings.boxCols - 1) {
                        slider.append(
                            $('<div class="nivo-box" name="' + cols + '" rel="' + rows + '"><img src="' + vars.currentImage.attr('src') + '" style="position:absolute; width:' + slider.width() + 'px; height:auto; display:block; top:-' + (boxHeight * rows) + 'px; left:-' + (boxWidth * cols) + 'px;" /></div>').css({
                                opacity: 0,
                                left: (boxWidth * cols) + 'px',
                                top: (boxHeight * rows) + 'px',
                                width: (slider.width() - (boxWidth * cols)) + 'px'

                            })
                        );
                        $('.nivo-box[name="' + cols + '"]', slider).height($('.nivo-box[name="' + cols + '"] img', slider).height() + 'px');
                    } else {
                        slider.append(
                            $('<div class="nivo-box" name="' + cols + '" rel="' + rows + '"><img src="' + vars.currentImage.attr('src') + '" style="position:absolute; width:' + slider.width() + 'px; height:auto; display:block; top:-' + (boxHeight * rows) + 'px; left:-' + (boxWidth * cols) + 'px;" /></div>').css({
                                opacity: 0,
                                left: (boxWidth * cols) + 'px',
                                top: (boxHeight * rows) + 'px',
                                width: boxWidth + 'px'
                            })
                        );
                        $('.nivo-box[name="' + cols + '"]', slider).height($('.nivo-box[name="' + cols + '"] img', slider).height() + 'px');
                    }
                }
            }

            //sliderImg.stop().animate({
            //    height: $(vars.currentImage).height()
            //}, settings.animSpeed);
        };

        // Private run method
        var nivoRun = function (slider, kids, settings, nudge) {
            // Get our vars
            var vars = slider.data('nivo:vars');

            // Trigger the lastSlide callback
            if (vars && (vars.currentSlide === vars.totalSlides - 1)) {
                settings.lastSlide.call(this);
            }

            // Stop
            if ((!vars || vars.stop) && !nudge) { return false; }

            // Trigger the beforeChange callback
            settings.beforeChange.call(this);

            // Set current background before change
            if (!nudge) {
                sliderImg.attr('src', vars.currentImage.attr('src'));
            } else {
                if (nudge === 'prev') {
                    sliderImg.attr('src', vars.currentImage.attr('src'));
                }
                if (nudge === 'next') {
                    sliderImg.attr('src', vars.currentImage.attr('src'));
                }
            }

            vars.currentSlide++;
            // Trigger the slideshowEnd callback
            if (vars.currentSlide === vars.totalSlides) {
                vars.currentSlide = 0;
                settings.slideshowEnd.call(this);
            }
            if (vars.currentSlide < 0) { vars.currentSlide = (vars.totalSlides - 1); }
            // Set vars.currentImage
            if ($(kids[vars.currentSlide]).is('img')) {
                vars.currentImage = $(kids[vars.currentSlide]);
            } else {
                vars.currentImage = $(kids[vars.currentSlide]).find('img:first');
            }

            // Set active links
            if (settings.controlNav) {
                $('li', vars.controlNavEl).removeClass('active');
                $('li:eq(' + vars.currentSlide + ')', vars.controlNavEl).addClass('active');
            }

            // Process caption
            processCaption(settings);

            // Remove any slices from last transition
            $('.nivo-slice', slider).remove();

            // Remove any boxes from last transition
            $('.nivo-box', slider).remove();

            var currentEffect = settings.effect,
                anims = '';

            // Generate random effect
            if (settings.effect === 'random') {
                anims = new Array('sliceDownRight', 'sliceDownLeft', 'sliceUpRight', 'sliceUpLeft', 'sliceUpDown', 'sliceUpDownLeft', 'fold', 'fade',
                'boxRandom', 'boxRain', 'boxRainReverse', 'boxRainGrow', 'boxRainGrowReverse');
                currentEffect = anims[Math.floor(Math.random() * (anims.length + 1))];
                if (currentEffect === undefined) { currentEffect = 'fade'; }
            }

            // Run random effect from specified set (eg: effect:'fold,fade')
            if (settings.effect.indexOf(',') !== -1) {
                anims = settings.effect.split(',');
                currentEffect = anims[Math.floor(Math.random() * (anims.length))];
                if (currentEffect === undefined) { currentEffect = 'fade'; }
            }

            // Custom transition as defined by "data-transition" attribute
            if (vars.currentImage.attr('data-transition')) {
                currentEffect = vars.currentImage.attr('data-transition');
            }

            // Run effects
            vars.running = true;
            var timeBuff = 0,
                i = 0,
                slices = '',
                firstSlice = '',
                totalBoxes = '',
                boxes = '';

            if (currentEffect === 'sliceDown' || currentEffect === 'sliceDownRight' || currentEffect === 'sliceDownLeft') {
                createSlices(slider, settings, vars);
                timeBuff = 0;
                i = 0;
                slices = $('.nivo-slice', slider);
                if (currentEffect === 'sliceDownLeft') { slices = $('.nivo-slice', slider)._reverse(); }

                slices.each(function () {
                    var slice = $(this);
                    slice.css({ 'top': '0px' });
                    if (i === settings.slices - 1) {
                        setTimeout(function () {
                            slice.animate({ opacity: '1.0' }, settings.animSpeed, '', function () { slider.trigger('nivo:animFinished'); });
                        }, (100 + timeBuff));
                    } else {
                        setTimeout(function () {
                            slice.animate({ opacity: '1.0' }, settings.animSpeed);
                        }, (100 + timeBuff));
                    }
                    timeBuff += 50;
                    i++;
                });
            } else if (currentEffect === 'sliceUp' || currentEffect === 'sliceUpRight' || currentEffect === 'sliceUpLeft') {
                createSlices(slider, settings, vars);
                timeBuff = 0;
                i = 0;
                slices = $('.nivo-slice', slider);
                if (currentEffect === 'sliceUpLeft') { slices = $('.nivo-slice', slider)._reverse(); }

                slices.each(function () {
                    var slice = $(this);
                    slice.css({ 'bottom': '0px' });
                    if (i === settings.slices - 1) {
                        setTimeout(function () {
                            slice.animate({ opacity: '1.0' }, settings.animSpeed, '', function () { slider.trigger('nivo:animFinished'); });
                        }, (100 + timeBuff));
                    } else {
                        setTimeout(function () {
                            slice.animate({ opacity: '1.0' }, settings.animSpeed);
                        }, (100 + timeBuff));
                    }
                    timeBuff += 50;
                    i++;
                });
            } else if (currentEffect === 'sliceUpDown' || currentEffect === 'sliceUpDownRight' || currentEffect === 'sliceUpDownLeft') {
                createSlices(slider, settings, vars);
                timeBuff = 0;
                i = 0;
                var v = 0;
                slices = $('.nivo-slice', slider);
                if (currentEffect === 'sliceUpDownLeft') { slices = $('.nivo-slice', slider)._reverse(); }

                slices.each(function () {
                    var slice = $(this);
                    if (i === 0) {
                        slice.css('top', '0px');
                        i++;
                    } else {
                        slice.css('bottom', '0px');
                        i = 0;
                    }

                    if (v === settings.slices - 1) {
                        setTimeout(function () {
                            slice.animate({ opacity: '1.0' }, settings.animSpeed, '', function () { slider.trigger('nivo:animFinished'); });
                        }, (100 + timeBuff));
                    } else {
                        setTimeout(function () {
                            slice.animate({ opacity: '1.0' }, settings.animSpeed);
                        }, (100 + timeBuff));
                    }
                    timeBuff += 50;
                    v++;
                });
            } else if (currentEffect === 'fold') {
                createSlices(slider, settings, vars);
                timeBuff = 0;
                i = 0;

                $('.nivo-slice', slider).each(function () {
                    var slice = $(this);
                    var origWidth = slice.width();
                    slice.css({ top: '0px', width: '0px' });
                    if (i === settings.slices - 1) {
                        setTimeout(function () {
                            slice.animate({ width: origWidth, opacity: '1.0' }, settings.animSpeed, '', function () { slider.trigger('nivo:animFinished'); });
                        }, (100 + timeBuff));
                    } else {
                        setTimeout(function () {
                            slice.animate({ width: origWidth, opacity: '1.0' }, settings.animSpeed);
                        }, (100 + timeBuff));
                    }
                    timeBuff += 50;
                    i++;
                });
            } else if (currentEffect === 'fade') {
                createSlices(slider, settings, vars);

                firstSlice = $('.nivo-slice:first', slider);
                firstSlice.css({
                    'width': slider.width() + 'px'
                });

                firstSlice.animate({ opacity: '1.0' }, (settings.animSpeed * 2), '', function () { slider.trigger('nivo:animFinished'); });
            } else if (currentEffect === 'slideInRight') {
                createSlices(slider, settings, vars);

                firstSlice = $('.nivo-slice:first', slider);
                firstSlice.css({
                    'width': '0px',
                    'opacity': '1'
                });

                firstSlice.animate({ width: slider.width() + 'px' }, (settings.animSpeed * 2), '', function () { slider.trigger('nivo:animFinished'); });
            } else if (currentEffect === 'slideInLeft') {
                createSlices(slider, settings, vars);

                firstSlice = $('.nivo-slice:first', slider);
                firstSlice.css({
                    'width': '0px',
                    'opacity': '1',
                    'left': '',
                    'right': '0px'
                });

                firstSlice.animate({ width: slider.width() + 'px' }, (settings.animSpeed * 2), '', function () {
                    // Reset positioning
                    firstSlice.css({
                        'left': '0px',
                        'right': ''
                    });
                    slider.trigger('nivo:animFinished');
                });
            } else if (currentEffect === 'boxRandom') {
                createBoxes(slider, settings, vars);

                totalBoxes = settings.boxCols * settings.boxRows;
                i = 0;
                timeBuff = 0;

                boxes = shuffle($('.nivo-box', slider));
                boxes.each(function () {
                    var box = $(this);
                    if (i === totalBoxes - 1) {
                        setTimeout(function () {
                            box.animate({ opacity: '1' }, settings.animSpeed, '', function () { slider.trigger('nivo:animFinished'); });
                        }, (100 + timeBuff));
                    } else {
                        setTimeout(function () {
                            box.animate({ opacity: '1' }, settings.animSpeed);
                        }, (100 + timeBuff));
                    }
                    timeBuff += 20;
                    i++;
                });
            } else if (currentEffect === 'boxRain' || currentEffect === 'boxRainReverse' || currentEffect === 'boxRainGrow' || currentEffect === 'boxRainGrowReverse') {
                createBoxes(slider, settings, vars);

                totalBoxes = settings.boxCols * settings.boxRows;
                i = 0;
                timeBuff = 0;

                // Split boxes into 2D array
                var rowIndex = 0;
                var colIndex = 0;
                var box2Darr = [];
                box2Darr[rowIndex] = [];
                boxes = $('.nivo-box', slider);
                if (currentEffect === 'boxRainReverse' || currentEffect === 'boxRainGrowReverse') {
                    boxes = $('.nivo-box', slider)._reverse();
                }
                boxes.each(function () {
                    box2Darr[rowIndex][colIndex] = $(this);
                    colIndex++;
                    if (colIndex === settings.boxCols) {
                        rowIndex++;
                        colIndex = 0;
                        box2Darr[rowIndex] = [];
                    }
                });

                // Run animation
                for (var cols = 0; cols < (settings.boxCols * 2) ; cols++) {
                    var prevCol = cols;
                    for (var rows = 0; rows < settings.boxRows; rows++) {
                        if (prevCol >= 0 && prevCol < settings.boxCols) {
                            /* Due to some weird JS bug with loop vars 
                            being used in setTimeout, this is wrapped
                            with an anonymous function call */
                            (function (row, col, time, i, totalBoxes) {
                                var box = $(box2Darr[row][col]);
                                var w = box.width();
                                var h = box.height();
                                if (currentEffect === 'boxRainGrow' || currentEffect === 'boxRainGrowReverse') {
                                    box.width(0).height(0);
                                }
                                if (i === totalBoxes - 1) {
                                    setTimeout(function () {
                                        box.animate({ opacity: '1', width: w, height: h }, settings.animSpeed / 1.3, '', function () { slider.trigger('nivo:animFinished'); });
                                    }, (100 + time));
                                } else {
                                    setTimeout(function () {
                                        box.animate({ opacity: '1', width: w, height: h }, settings.animSpeed / 1.3);
                                    }, (100 + time));
                                }
                            })(rows, prevCol, timeBuff, i, totalBoxes);
                            i++;
                        }
                        prevCol--;
                    }
                    timeBuff += 100;
                }
            }
        };

        // Shuffle an array
        var shuffle = function (arr) {
            for (var j, x, i = arr.length; i; j = parseInt(Math.random() * i, 10), x = arr[--i], arr[i] = arr[j], arr[j] = x);
            return arr;
        };

        // For debugging
        var trace = function (msg) {
            if (this.console && typeof console.log !== 'undefined') { console.log(msg); }
        };

        // Start / Stop
        this.stop = function () {
            if (!$(element).data('nivo:vars').stop) {
                $(element).data('nivo:vars').stop = true;
                trace('Stop Slider');
            }
        };

        this.start = function () {
            if ($(element).data('nivo:vars').stop) {
                $(element).data('nivo:vars').stop = false;
                trace('Start Slider');
            }
        };

        // Trigger the afterLoad callback
        settings.afterLoad.call(this);

        return this;
    };

    $.fn.nivoSlider = function (options) {
        return this.each(function (key, value) {
            var element = $(this);
            // Return early if this element already has a plugin instance
            if (element.data('nivoslider')) { return element.data('nivoslider'); }
            // Pass options to plugin constructor
            var nivoslider = new NivoSlider(this, options);
            // Store plugin object in this element's data
            element.data('nivoslider', nivoslider);
        });
    };

    //Default settings
    $.fn.nivoSlider.defaults = {
        effect: 'random',
        slices: 15,
        boxCols: 8,
        boxRows: 4,
        animSpeed: 500,
        pauseTime: 3000,
        startSlide: 0,
        directionNav: true,
        controlNav: true,
        controlNavThumbs: false,
        pauseOnHover: true,
        manualAdvance: false,
        prevText: 'Prev',
        nextText: 'Next',
        randomStart: false,
        beforeChange: function () { },
        afterChange: function () { },
        slideshowEnd: function () { },
        lastSlide: function () { },
        afterLoad: function () { }
    };

    $.fn._reverse = [].reverse;

})(jQuery);
///#source 1 1 /template/steamcn_metro/src/js/jquery.sliderTabs.js
/*
 * jQuery SliderTabs v1.1
 * http://lopatin.github.com/sliderTabs
 *
 * Copyright 2012, Alex Lopatin
 * Free to use under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 *
 */


 (function( $ ){
	/*
	 * The sliderTabs tabs class
	 */
	$.sliderTabs = function(container, options){
		var plugin = this;

		var defaults = {
			autoplay: false,
			tabArrowWidth: 35,
			classes: {
				leftTabArrow: '',
				panel: '',
				panelActive: '',
				panelsContainer: '',
				rightTabArrow: '',
				tab: '',
				tabActive: '',	
				tabsList: ''
			},
			defaultTab: 1,
			height: null,
			indicators: false,
			mousewheel: true,
			position: "top",
			panelArrows: false,
			panelArrowsShowOnHover: false,
			tabs: true,
			tabHeight: 30,
			tabArrows: true,
			tabSlideLength: 100,
			tabSlideSpeed: 200,
			transition: 'slide',
			transitionEasing: 'easeOutCubic',
			transitionSpeed: 500,
			width: null
		};

		// jQuery objects of important elements
		var $container = $(container),
			$indicators,
			$tabsList,
			$contentDivs,
			$tabsListContainer,
			$tabsListWrapper,
			$contentDivsContainer,
			$leftTabArrow,
			$rightTabArrow,
			$leftPanelArrow,
			$rightPanelArrow;

		// Locks to stop out of sync behavior
		var selectLock = false,
			heightLock = true;
		
		var settings, minMargin;

		// Index of currently selected tab
		plugin.selectedTab = defaults.defaultTab;

		plugin.init = function(){
			settings = plugin.settings = $.extend({}, defaults, options);
			$container.addClass('ui-slider-tabs');

			/* 
			 * Rebuild structure of container
			 */
			$contentDivs = $container.children("div").addClass('ui-slider-tab-content').remove();
			
			// Tabs
			$tabsList = $container.children("ul").addClass('ui-slider-tabs-list').remove();
			$tabsList.children("li").remove().appendTo($tabsList);
			plugin.count = $tabsList.children('li').length;
			$tabsListWrapper = $("<div class='ui-slider-tabs-list-wrapper'>");
			$tabsListContainer = $("<div class='ui-slider-tabs-list-container'>").append($tabsList).appendTo($tabsListWrapper);
			$tabsListContainer.find('li').css('height', settings.tabHeight+2);
			$tabsListContainer.find('li a').css('height', settings.tabHeight+2);
			
			// Tab arrows
			$leftTabArrow = $("<a href='#' class='ui-slider-left-arrow'><div></div></a>").css({
				'width': settings.tabArrowWidth,
				'height': settings.tabHeight+2
			}).appendTo($tabsListContainer).click(function(e){
				plugin.slideTabs('right', settings.tabSlideLength);
				return false;
			});
			$rightTabArrow = $("<a href='#' class='ui-slider-right-arrow'><div></div></a>").css({
				'width': settings.tabArrowWidth,
				'height': settings.tabHeight+2
			}).appendTo($tabsListContainer).click(function(e){
				plugin.slideTabs('left', settings.tabSlideLength);
				return false;
			});

			// Content container
			$contentDivsContainer = $("<div class='ui-slider-tabs-content-container'>").append($contentDivs);

			// Position the tabs on top or bottom
			if(settings.position == 'bottom')
				$container.append($contentDivsContainer).append($tabsListWrapper.addClass('bottom'));
			else
				$container.append($tabsListWrapper).append($contentDivsContainer);
			

			if(settings.width)
				$container.width(parseInt(settings.width));
			if(settings.height)
				$contentDivsContainer.height(parseInt(settings.height)- settings.tabHeight);

			// Create and show indicators
			if(settings.indicators)
				plugin.showIndicators();
			

			// Select default tab
			plugin.selectTab(settings.defaultTab);
			plugin.slideTabs('left', 0);

			reorderPanels();

			resizePanels();

			// When tab is clicked
			$container.delegate('.ui-slider-tabs-list li a', 'click', function(){
				if(!$(this).parent().hasClass('selected') && !selectLock){
					plugin.selectTab($(this).parent());
				}
				return false;
			});

			// When indicator is clicked
			if($indicators)
				$indicators.delegate('.ui-slider-tabs-indicator', 'click', function(){
					if(!$(this).hasClass('selected') && !selectLock)
						plugin.selectTab($(this).index()+1);
				});

			// Set classes
			$.each(settings.classes, function(i, c){
				switch(i){
					case 'leftTabArrow': 
						$leftTabArrow.addClass(c);
						break;
					case 'rightTabArrow':
						$rightTabArrow.addClass(c);
						break;
					case 'panel':
						$contentDivs.addClass(c);
						break;
					case 'panelsContainer':
						$contentDivsContainer.addClass(c);
						break;
					case 'tab':
						$tabsList.find('li').addClass(c);
						break;
					case 'tabsList':
						$tabsList.addClass(c);
						break;
					default:
						break;
				}
			});

			// Panel arrows
			// Creates them if they don't exist
			if(settings.panelArrows)
				positionPanelArrows();

			if(settings.panelArrowsShowOnHover){
				if($leftPanelArrow)
					$leftPanelArrow.addClass('showOnHover');
				if($rightPanelArrow)
					$rightPanelArrow.addClass('showOnHover');
			}

			$contentDivsContainer.resize(positionPanelArrows);

			// Make responsive to changes in dimensions
			$tabsListWrapper.resize(function(){
				resizeTabsList();
				resizePanels();
			});
			
			// Resize content container height if inner panels change
			setInterval(function(){
				var $panel = $contentDivsContainer.children('.selected');
				if($panel.outerHeight() > $contentDivsContainer.outerHeight() && heightLock)
					resizeContentContainer($panel);
			}, 100);

			resizeTabsList();

			// Hide tabs wrapper if option if false
			if(!settings.tabs)
				$tabsListWrapper.hide();

			// Auto play
			if(settings.autoplay)
				setInterval(plugin.next, settings.autoplay);

			// Panel arrows

		    // Mousehweel
            if(settings.mousewheel) {
                $container.bind('mousewheel', function(event, delta, deltaX, deltaY) {
                    if (delta > 0)
                        plugin.next();
                    else if (delta < 0)
                        plugin.prev();
                    return false;
                });
            }
		}

		/*
		 * Public methods
		 */

		// Select tab
		// param: tab is a tab index (1 ... n) or jQuery object of tab li element
		plugin.selectTab = function(tab){
			heightLock = false;

			// Find $targetPanel, the panel to show
			var $clicked = (typeof tab === 'number') ? $tabsList.children("li:nth-child("+tab+")") : tab;
			var targetId = ($clicked.find('a').attr('href')).substr(1);
			var $targetPanel = $contentDivsContainer.children("#"+targetId);

			// Update selected tab
			plugin.selectedTab = (typeof tab === 'number') ? tab : tab.index()+1;

			// Resize the main contant container to the size of $targetPanel
			resizeContentContainer($targetPanel);

			// Lock selections until transitions finished
			selectLock = true;

			// Direction to slide panel on hide
			var direction = ($tabsList.find('.selected').index() < $clicked.index()) ? 'left' : 'right';
			
			// Update selected classes
			$clicked.siblings().removeClass('selected');
			if(settings.classes.tabActive != '') $clicked.siblings().removeClass(settings.classes.tabActive);
			$clicked.addClass('selected').addClass(settings.classes.tabActive);
			
			// Hide and show appropriate panels
			hidePanel($contentDivsContainer.children(".ui-slider-tab-content:visible"), direction);
			showPanel($targetPanel);

			// Slide tabs so that they fit in $tabsListContainer
			fitTabInContainer($clicked);

			// Select the proper indicator
			selectIndicator();
		};

		// Select the next (right) panel
		plugin.next = function(){
			if(!selectLock){
				if(plugin.count === plugin.selectedTab)
					plugin.selectTab(1);
				else plugin.selectTab(plugin.selectedTab+1);
			}	
		};

		// Select the previous panel
		plugin.prev = function(){
			if(!selectLock){
				if(plugin.selectedTab === 1)
					plugin.selectTab(plugin.count);
				else plugin.selectTab(plugin.selectedTab-1);
			}
		};

		// Slide tabs left/right within $tabsListContainer
		plugin.slideTabs = function(direction, length){
			var margin = parseInt($tabsList.css('margin-left'));
			var newMargin = margin;

			// Reset 'edge' classes on tab arrows
			$leftTabArrow.removeClass('edge');
			$rightTabArrow.removeClass('edge');

			// Calculate delta to slide by
			if(direction=='right') newMargin += length;
			else if(direction=='left') newMargin -= length;
			if(newMargin >= 0) {
				newMargin = 0;
				$leftTabArrow.addClass('edge');
			}
			else if(newMargin <= minMargin){
				newMargin = minMargin;
				$rightTabArrow.addClass('edge');
			}

			// Animate
			$tabsList.animate({'margin-left': newMargin}, settings.tabSlideSpeed);
		};

		// Show panel indicators
		// Create indicators if they don't exist yet
		plugin.showIndicators = function(){
			if(!$indicators){
				$indicators = $("<div class='ui-slider-tabs-indicator-container'>");
				for(var i = 0; i < $contentDivs.length; i++){
					$indicators.append("<div class='ui-slider-tabs-indicator'></div>");
				}
				$contentDivsContainer.append($indicators);
			}
			else
				$indicators.show();
		};

		// Hide panel indicators
		plugin.hideIndicators = function(){
			if($indicators)
				$indicators.hide();
		};

		// Show arrows that slide tabs left and right
		plugin.showTabArrows = function(){
			if(!settings.tabArrows)
				return;
			$leftTabArrow.show();
			$rightTabArrow.show();
			$tabsListContainer.css('margin', '0 '+settings.tabArrowWidth+'px');
		};

		// Hide arrows that slide tabs left and right
		plugin.hideTabArrows = function(){
			$leftTabArrow.hide();
			$rightTabArrow.hide();
			$tabsListContainer.css('margin', '0');
		};

		// Show panel arrows
		plugin.showPanelArrows = function(){
			if($leftPanelArrow) $leftPanelArrow.show();
			if($rightPanelArrow) $rightPanelArrow.show();
		};

		// Hide panel arrows
		plugin.hidePanelArrows = function(){
			if($leftPanelArrow) $leftPanelArrow.hide();
			if($rightPanelArrow) $rightPanelArrow.hide();
		};

		/*
		 * Private methods
		 */

		// Add the selected class to the plugin.selectedTab tab. Remove from all others.
		var selectIndicator = function(){
			if(settings.indicators && $indicators){
				var $indicator = $indicators.children("div:nth-child("+plugin.selectedTab+")");
				$indicator.siblings().removeClass('selected');
				$indicator.addClass('selected');
			}	
		};

		// 	Slide tabs inside of $tabsListContainer so that the selected one fits inside
		var fitTabInContainer = function(tab){
			var tabOffset = tab.offset(),
				containerOffset = $tabsListContainer.offset(),
				leftOffset = tabOffset.left - containerOffset.left,
				rightOffset = (containerOffset.left + $tabsListContainer.outerWidth()) - (tabOffset.left + tab.outerWidth() );
			
			if(leftOffset < 0)
				plugin.slideTabs('right', -leftOffset);
			else if(rightOffset < 0)
				plugin.slideTabs('left', -rightOffset);
		};

		// Reposition content panels so that they are ready to be transitioned in and out.
		// This depends on whether the transition is set to slide or fade
		var reorderPanels = function(){
			// Position content divs
			if(settings.transition == 'slide')
				// Move panels left/right basedon their index relative to the selected panel
				$tabsList.children('li').each(function(index, el){
					var selectedIndex = $tabsList.children('.selected').index(),
						thisIndex = $(el).index();
					var panel = $contentDivsContainer.children('#'+$(el).find('a').attr('href').substr(1));
					if(selectedIndex < thisIndex)
						panel.css({left: $contentDivsContainer.width()+'px'});
					else if(selectedIndex > thisIndex)
						panel.css({left: '-'+$contentDivsContainer.width()+'px'});
					else
						panel.addClass(settings.classes.panelActive);
				});
			
			if(settings.transition == 'fade')
				// Set opacity to correct value for non selected panels.
				$tabsList.children('li').each(function(index, el){
					var selectedIndex = $tabsList.children('.selected').index(),
						thisIndex = $(el).index();
					var panel = $contentDivsContainer.children('#'+$(el).find('a').attr('href').substr(1));
					if(selectedIndex != thisIndex)
						panel.css({opacity: 0});
					else
						panel.addClass(settings.classes.panelActive);
				});
		};
		
		// Object determining css properties to be animated to based on various actions, transitions, and directions
		var panelAnimationCSS = function(width){
			return {
					hide: {
						slideleft: {
							left: '-'+width+'px'
						},
						slideright: {
							left: width+'px'
						},
						fade: {
							opacity: 0
						}
					},
					show: {
						slide: {
							left: 0
						},
						fade: {
							opacity: 1
						}
					}
				}
		};
		
		// Transition out the passed in panel.
		// param: 	panel is the jQuery object of the panel to be hidden
		//			direction is either 'left' or 'right' for sliding transitions
		var hidePanel = function(panel, direction){
			// Calculate correct key in panelAnimationCSS
			if(settings.transition == 'slide')
				var trans = 'slide'+direction;
			else var trans = settings.transition;

			// Animate the panel out
			panel.animate(panelAnimationCSS($contentDivsContainer.width())['hide'][trans], settings.transitionSpeed, settings.transitionEasing, function(){
				panel.hide();
				panel.removeClass('selected');
				//if(settings.classes.panelActive != '') panel.removeClass(settings.classes.panelActive);
				selectLock = false;
				reorderPanels();
			});
		};

		// Transition in the parameter panel
		// param: 	panel is the jQuery object of the panel to be shown
		var showPanel = function(panel){
			// Show first
			panel.show();
			panel.addClass(settings.classes.panelActive).addClass('selected');

			// Then animate css properties
			panel.animate(panelAnimationCSS($contentDivsContainer.width())['show'][settings.transition], settings.transitionSpeed, settings.transitionEasing, function(){
				selectLock = false;
				heightLock = true;
				reorderPanels();
			});
		};

		// Animate the height of the content container to height target
		// params:  target (int) is the new height
		var resizeContentContainer = function(target){
			if(!settings.height)
				$contentDivsContainer.animate({
					height: actualHeight(target)
				}, 200);
		};

		// Position the panel arrows
		var positionPanelArrows = function(){
			if(settings.panelArrows){
				// Initialize them if you need to
				if(!$leftPanelArrow && !$rightPanelArrow){
					$leftPanelArrow = $("<div class='ui-slider-tabs-leftPanelArrow'>").click(function(){
						plugin.prev();
					});
					$rightPanelArrow = $("<div class='ui-slider-tabs-rightPanelArrow'>").click(function(){
						plugin.next();
					});

					$leftPanelArrow.appendTo($contentDivsContainer);
					$rightPanelArrow.appendTo($contentDivsContainer);
				}

				// Set correct CSS 'top' attribute of each panel arrow
				$rightPanelArrow.css({
					"top": $contentDivsContainer.height()/2 - $rightPanelArrow.outerHeight()/2
				});
				$leftPanelArrow.css({
					"top": $contentDivsContainer.height()/2 - $leftPanelArrow.outerHeight()/2
				});
			}
		};

		// Change the width of $tabsList to the sum of the outer widths of all tabs
		var resizeTabsList = function(){
			// Calculate total width
			var width = 0;
			$tabsList.children().each(function(index, element){
				width += $(element).outerWidth(true);
			});
			// Set new width of $tabsList
			$tabsList.width(width);

			// Update minMargin. Hide tab arrows if no overflow
			if($tabsListContainer.width() < width && settings.tabArrows){
				plugin.showTabArrows();
				minMargin = $tabsListContainer.width() - width;
			}
			else plugin.hideTabArrows();
		}

		// Resize indiviual panels to the width of the new container
		var resizePanels = function(){
			$contentDivs.width($contentDivsContainer.width() - ($contentDivs.outerWidth() - $contentDivs.width()));
		};

		// Get height of a hidden element
		var actualHeight = function(element){
			var prevCSS = {
				'display': element.css('display'),
				'left': element.css('left'),
				'position': element.css('position')
			};
			element.css({
				'display': 'normal',
				'left': -5000,
				'position': 'absolute'
			});
			var height = element.outerHeight();
			element.css(prevCSS);
			return height;
		};
		

		// Initialize the plugin
		plugin.init();
	};

	/*
	 * Handle input. Call public functions and initializers
	 */
	$.fn.sliderTabs = function( data ) {
		return this.each(function(){
			var _this = $(this),
				plugin = _this.data('sliderTabs');

			// Method calling logic
		    if (!plugin) {
		    	// If no plugin, initialize it
				plugin = new $.sliderTabs(this, data);
				_this.data('sliderTabs', plugin);
				return plugin;
			}
			if (plugin.methods[data]){
				// If plugin exists, call a public method
				return plugin.methods[ data ].apply( this, Array.prototype.slice.call( arguments, 1 ));
			}
		});
  	};
})( jQuery );





/*
 * Additional easing functions
 * Taken from jQuery UI source code
 * 
 * https://github.com/jquery/jquery-ui
 */

$.extend($.easing,
{
    def: 'easeOutQuad',
    swing: function (x, t, b, c, d) {
        //alert($.easing.default);
        return $.easing[$.easing.def](x, t, b, c, d);
    },
    easeInQuad: function (x, t, b, c, d) {
        return c*(t/=d)*t + b;
    },
    easeOutQuad: function (x, t, b, c, d) {
        return -c *(t/=d)*(t-2) + b;
    },
    easeInOutQuad: function (x, t, b, c, d) {
        if ((t/=d/2) < 1) return c/2*t*t + b;
        return -c/2 * ((--t)*(t-2) - 1) + b;
    },
    easeInCubic: function (x, t, b, c, d) {
        return c*(t/=d)*t*t + b;
    },
    easeOutCubic: function (x, t, b, c, d) {
        return c*((t=t/d-1)*t*t + 1) + b;
    },
    easeInOutCubic: function (x, t, b, c, d) {
        if ((t/=d/2) < 1) return c/2*t*t*t + b;
        return c/2*((t-=2)*t*t + 2) + b;
    },
    easeInQuart: function (x, t, b, c, d) {
        return c*(t/=d)*t*t*t + b;
    },
    easeOutQuart: function (x, t, b, c, d) {
        return -c * ((t=t/d-1)*t*t*t - 1) + b;
    },
    easeInOutQuart: function (x, t, b, c, d) {
        if ((t/=d/2) < 1) return c/2*t*t*t*t + b;
        return -c/2 * ((t-=2)*t*t*t - 2) + b;
    },
    easeInQuint: function (x, t, b, c, d) {
        return c*(t/=d)*t*t*t*t + b;
    },
    easeOutQuint: function (x, t, b, c, d) {
        return c*((t=t/d-1)*t*t*t*t + 1) + b;
    },
    easeInOutQuint: function (x, t, b, c, d) {
        if ((t/=d/2) < 1) return c/2*t*t*t*t*t + b;
        return c/2*((t-=2)*t*t*t*t + 2) + b;
    },
    easeInSine: function (x, t, b, c, d) {
        return -c * Math.cos(t/d * (Math.PI/2)) + c + b;
    },
    easeOutSine: function (x, t, b, c, d) {
        return c * Math.sin(t/d * (Math.PI/2)) + b;
    },
    easeInOutSine: function (x, t, b, c, d) {
        return -c/2 * (Math.cos(Math.PI*t/d) - 1) + b;
    },
    easeInExpo: function (x, t, b, c, d) {
        return (t==0) ? b : c * Math.pow(2, 10 * (t/d - 1)) + b;
    },
    easeOutExpo: function (x, t, b, c, d) {
        return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
    },
    easeInOutExpo: function (x, t, b, c, d) {
        if (t==0) return b;
        if (t==d) return b+c;
        if ((t/=d/2) < 1) return c/2 * Math.pow(2, 10 * (t - 1)) + b;
        return c/2 * (-Math.pow(2, -10 * --t) + 2) + b;
    },
    easeInCirc: function (x, t, b, c, d) {
        return -c * (Math.sqrt(1 - (t/=d)*t) - 1) + b;
    },
    easeOutCirc: function (x, t, b, c, d) {
        return c * Math.sqrt(1 - (t=t/d-1)*t) + b;
    },
    easeInOutCirc: function (x, t, b, c, d) {
        if ((t/=d/2) < 1) return -c/2 * (Math.sqrt(1 - t*t) - 1) + b;
        return c/2 * (Math.sqrt(1 - (t-=2)*t) + 1) + b;
    },
    easeInElastic: function (x, t, b, c, d) {
        var s=1.70158;var p=0;var a=c;
        if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
        if (a < Math.abs(c)) { a=c; var s=p/4; }
        else var s = p/(2*Math.PI) * Math.asin (c/a);
        return -(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
    },
    easeOutElastic: function (x, t, b, c, d) {
        var s=1.70158;var p=0;var a=c;
        if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
        if (a < Math.abs(c)) { a=c; var s=p/4; }
        else var s = p/(2*Math.PI) * Math.asin (c/a);
        return a*Math.pow(2,-10*t) * Math.sin( (t*d-s)*(2*Math.PI)/p ) + c + b;
    },
    easeInOutElastic: function (x, t, b, c, d) {
        var s=1.70158;var p=0;var a=c;
        if (t==0) return b;  if ((t/=d/2)==2) return b+c;  if (!p) p=d*(.3*1.5);
        if (a < Math.abs(c)) { a=c; var s=p/4; }
        else var s = p/(2*Math.PI) * Math.asin (c/a);
        if (t < 1) return -.5*(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
        return a*Math.pow(2,-10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )*.5 + c + b;
    },
    easeInBack: function (x, t, b, c, d, s) {
        if (s == undefined) s = 1.70158;
        return c*(t/=d)*t*((s+1)*t - s) + b;
    },
    easeOutBack: function (x, t, b, c, d, s) {
        if (s == undefined) s = 1.70158;
        return c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;
    },
    easeInOutBack: function (x, t, b, c, d, s) {
        if (s == undefined) s = 1.70158;
        if ((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b;
        return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;
    },
    easeInBounce: function (x, t, b, c, d) {
        return c - $.easing.easeOutBounce (x, d-t, 0, c, d) + b;
    },
    easeOutBounce: function (x, t, b, c, d) {
        if ((t/=d) < (1/2.75)) {
            return c*(7.5625*t*t) + b;
        } else if (t < (2/2.75)) {
            return c*(7.5625*(t-=(1.5/2.75))*t + .75) + b;
        } else if (t < (2.5/2.75)) {
            return c*(7.5625*(t-=(2.25/2.75))*t + .9375) + b;
        } else {
            return c*(7.5625*(t-=(2.625/2.75))*t + .984375) + b;
        }
    },
    easeInOutBounce: function (x, t, b, c, d) {
        if (t < d/2) return $.easing.easeInBounce (x, t*2, 0, c, d) * .5 + b;
        return $.easing.easeOutBounce (x, t*2-d, 0, c, d) * .5 + c*.5 + b;
    }
});





/*
 * The following is the jQuery Mousewheel plugin. Full credit goes to 
 * Brandon Aaron. (https://github.com/brandonaaron/jquery-mousewheel)
 * /


/*! Copyright (c) 2011 Brandon Aaron (http://brandonaaron.net)
 * Licensed under the MIT License (LICENSE.txt).
 *
 * Thanks to: http://adomas.org/javascript-mouse-wheel/ for some pointers.
 * Thanks to: Mathias Bank(http://www.mathias-bank.de) for a scope bug fix.
 * Thanks to: Seamus Leahy for adding deltaX and deltaY
 *
 * Version: 3.0.6
 * 
 * Requires: 1.2.2+
 */

(function($) {

var types = ['DOMMouseScroll', 'mousewheel'];

if ($.event.fixHooks) {
    for ( var i=types.length; i; ) {
        $.event.fixHooks[ types[--i] ] = $.event.mouseHooks;
    }
}

$.event.special.mousewheel = {
    setup: function() {
        if ( this.addEventListener ) {
            for ( var i=types.length; i; ) {
                this.addEventListener( types[--i], handler, false );
            }
        } else {
            this.onmousewheel = handler;
        }
    },
    
    teardown: function() {
        if ( this.removeEventListener ) {
            for ( var i=types.length; i; ) {
                this.removeEventListener( types[--i], handler, false );
            }
        } else {
            this.onmousewheel = null;
        }
    }
};

$.fn.extend({
    mousewheel: function(fn) {
        return fn ? this.bind("mousewheel", fn) : this.trigger("mousewheel");
    },
    
    unmousewheel: function(fn) {
        return this.unbind("mousewheel", fn);
    }
});


function handler(event) {
    var orgEvent = event || window.event, args = [].slice.call( arguments, 1 ), delta = 0, returnValue = true, deltaX = 0, deltaY = 0;
    event = $.event.fix(orgEvent);
    event.type = "mousewheel";
    
    // Old school scrollwheel delta
    if ( orgEvent.wheelDelta ) { delta = orgEvent.wheelDelta/120; }
    if ( orgEvent.detail     ) { delta = -orgEvent.detail/3; }
    
    // New school multidimensional scroll (touchpads) deltas
    deltaY = delta;
    
    // Gecko
    if ( orgEvent.axis !== undefined && orgEvent.axis === orgEvent.HORIZONTAL_AXIS ) {
        deltaY = 0;
        deltaX = -1*delta;
    }
    
    // Webkit
    if ( orgEvent.wheelDeltaY !== undefined ) { deltaY = orgEvent.wheelDeltaY/120; }
    if ( orgEvent.wheelDeltaX !== undefined ) { deltaX = -1*orgEvent.wheelDeltaX/120; }
    
    // Add event and delta to the front of the arguments
    args.unshift(event, delta, deltaX, deltaY);
    
    return ($.event.dispatch || $.event.handle).apply(this, args);
}

})(jQuery);



/*
 * The following is the jQuery Resize plugin. Full credit goes to 
 * "Cowboy" Ben Alman. (https://github.com/cowboy/jquery-resize)
 * /

/*
 * jQuery resize event - v1.1 - 3/14/2010
 * http://benalman.com/projects/jquery-resize-plugin/
 * 
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */
(function($,h,c){var a=$([]),e=$.resize=$.extend($.resize,{}),i,k="setTimeout",j="resize",d=j+"-special-event",b="delay",f="throttleWindow";e[b]=250;e[f]=true;$.event.special[j]={setup:function(){if(!e[f]&&this[k]){return false}var l=$(this);a=a.add(l);$.data(this,d,{w:l.width(),h:l.height()});if(a.length===1){g()}},teardown:function(){if(!e[f]&&this[k]){return false}var l=$(this);a=a.not(l);l.removeData(d);if(!a.length){clearTimeout(i)}},add:function(l){if(!e[f]&&this[k]){return false}var n;function m(s,o,p){var q=$(this),r=$.data(this,d);r.w=o!==c?o:q.width();r.h=p!==c?p:q.height();n.apply(this,arguments)}if($.isFunction(l)){n=l;return m}else{n=l.handler;l.handler=m}}};function g(){i=h[k](function(){a.each(function(){var n=$(this),m=n.width(),l=n.height(),o=$.data(this,d);if(m!==o.w||l!==o.h){n.trigger(j,[o.w=m,o.h=l])}});g()},e[b])}})(jQuery,this);
///#source 1 1 /template/steamcn_metro/src/js/jQuery.XDomainRequest.js
/*!
 * jQuery-ajaxTransport-XDomainRequest - v1.0.3 - 2014-06-06
 * https://github.com/MoonScript/jQuery-ajaxTransport-XDomainRequest
 * Copyright (c) 2014 Jason Moon (@JSONMOON)
 * Licensed MIT (/blob/master/LICENSE.txt)
 */
(function(factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as anonymous module.
    define(['jquery'], factory);
  } else if (typeof exports === 'object') {
    // CommonJS
    module.exports = factory(require('jquery'));
  } else {
    // Browser globals.
    factory(jQuery);
  }
}(function($) {

// Only continue if we're on IE8/IE9 with jQuery 1.5+ (contains the ajaxTransport function)
if ($.support.cors || !$.ajaxTransport || !window.XDomainRequest) {
  return;
}

var httpRegEx = /^https?:\/\//i;
var getOrPostRegEx = /^get|post$/i;
var sameSchemeRegEx = new RegExp('^'+location.protocol, 'i');

// ajaxTransport exists in jQuery 1.5+
$.ajaxTransport('* text html xml json', function(options, userOptions, jqXHR) {
  
  // Only continue if the request is: asynchronous, uses GET or POST method, has HTTP or HTTPS protocol, and has the same scheme as the calling page
  if (!options.crossDomain || !options.async || !getOrPostRegEx.test(options.type) || !httpRegEx.test(options.url) || !sameSchemeRegEx.test(options.url)) {
    return;
  }

  var xdr = null;

  return {
    send: function(headers, complete) {
      var postData = '';
      var userType = (userOptions.dataType || '').toLowerCase();

      xdr = new XDomainRequest();
      if (/^\d+$/.test(userOptions.timeout)) {
        xdr.timeout = userOptions.timeout;
      }

      xdr.ontimeout = function() {
        complete(500, 'timeout');
      };

      xdr.onload = function() {
        var allResponseHeaders = 'Content-Length: ' + xdr.responseText.length + '\r\nContent-Type: ' + xdr.contentType;
        var status = {
          code: 200,
          message: 'success'
        };
        var responses = {
          text: xdr.responseText
        };
        try {
          if (userType === 'html' || /text\/html/i.test(xdr.contentType)) {
            responses.html = xdr.responseText;
          } else if (userType === 'json' || (userType !== 'text' && /\/json/i.test(xdr.contentType))) {
            try {
              responses.json = $.parseJSON(xdr.responseText);
            } catch(e) {
              status.code = 500;
              status.message = 'parseerror';
              //throw 'Invalid JSON: ' + xdr.responseText;
            }
          } else if (userType === 'xml' || (userType !== 'text' && /\/xml/i.test(xdr.contentType))) {
            var doc = new ActiveXObject('Microsoft.XMLDOM');
            doc.async = false;
            try {
              doc.loadXML(xdr.responseText);
            } catch(e) {
              doc = undefined;
            }
            if (!doc || !doc.documentElement || doc.getElementsByTagName('parsererror').length) {
              status.code = 500;
              status.message = 'parseerror';
              throw 'Invalid XML: ' + xdr.responseText;
            }
            responses.xml = doc;
          }
        } catch(parseMessage) {
          throw parseMessage;
        } finally {
          complete(status.code, status.message, responses, allResponseHeaders);
        }
      };

      // set an empty handler for 'onprogress' so requests don't get aborted
      xdr.onprogress = function(){};
      xdr.onerror = function() {
        complete(500, 'error', {
          text: xdr.responseText
        });
      };

      if (userOptions.data) {
        postData = ($.type(userOptions.data) === 'string') ? userOptions.data : $.param(userOptions.data);
      }
      xdr.open(options.type, options.url);
      xdr.send(postData);
    },
    abort: function() {
      if (xdr) {
        xdr.abort();
      }
    }
  };
});

}));

