window.dataLayer = window.dataLayer || [];
function gtag(){ dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-17880378-3', { 'send_page_view': false });

var loadingTimer;
var spinner = $('.dzlab_jzloading');
var isMiniProgram = window.__wxjs_environment === 'miniprogram' || navigator.userAgent.match(/miniprogram/i) || navigator.userAgent.match(/swan/i) || navigator.userAgent.match(/ToutiaoMicroApp/i);

(function() {
    Turbolinks.Location.prototype.isHTML = function() {
        return true;
    };
    Turbolinks.SnapshotRenderer.prototype.assignNewBody = function () {
        morphdom(document.body, this.newBody, {
            onBeforeElUpdated: function(fromEl, toEl) {
                return !fromEl.isEqualNode(toEl);
            }
        });
    };
    Turbolinks.SnapshotRenderer.prototype.activateNewBodyScriptElements = function () {
        evalScriptInOrder(document.body.querySelectorAll('script'));
    };
    Turbolinks.SnapshotRenderer.prototype.replaceBody = function () {
        var placeholders = this.relocateCurrentBodyPermanentElements();
        this.assignNewBody();
        this.activateNewBodyScriptElements();
        this.replacePlaceholderElementsWithClonedPermanentElements(placeholders);
    };
    var originalCacheSnapshot = Turbolinks.Controller.prototype.cacheSnapshot;
    var originalDefer = Turbolinks.defer;
    var noDefer = function(callback) { callback(); }
    Turbolinks.Controller.prototype.cacheSnapshot = function () {
        Turbolinks.defer = noDefer;
        originalCacheSnapshot.call(this, ...arguments);
        Turbolinks.defer = originalDefer;
    };
    Turbolinks.start();
    PullToRefresh.setPassiveMode(true);

    document.addEventListener('turbolinks:before-visit', function(e) {
        if (e.data.url.indexOf('jzsjiale_sms') > -1) {
            e.preventDefault();
            window.location.href = e.data.url;
        }
    });

    document.addEventListener('turbolinks:request-start', function(e) {
        loadingTimer = setTimeout(showLoading, 400);
        e.data.xhr.setRequestHeader('X-Turbolinks', '1');
    });

    document.addEventListener('turbolinks:request-end', function() {
        hideLoading();
    });

    document.addEventListener('turbolinks:before-cache', function() {
        var lg;
        if (window.lgData && (lg = window.lgData[document.body.getAttribute('lg-uid')])) {
            lg.destroy(true);
        }
        PullToRefresh.destroyAll();
        $.clearEventBindings();
    });

    var softReferrer;

    document.addEventListener('turbolinks:load', function(e) {
        gtag('config', 'UA-17880378-3', {
            'user_id': window.discuz_uid
        });
        if (isMiniProgram) {
            // replaceNotAllowedWords(document.body);
        }
        if (softReferrer) {
            var path = e.data.url.replace(window.location.protocol + '//' + window.location.hostname + (location.port && ':' + location.port), '');
            window._hmt.push(['_trackPageDuration']);
            window._hmt.push(['_setReferrerOverride', softReferrer]);
            window._hmt.push(['_trackPageview', path]);
        }
        softReferrer = e.data.url;

        $('.menu_btn').click(function(){
            if ($(this).hasClass('left_btn')) {
                window.history.go(-1);
            } else {
                $('.dzlab_menu').animate({'left':0},300);
                $('.menu_bg').css({display:'block'}).animate({'opacity':0.5},300);
            }
        });
        $('.menu_bg').click(function(){
            $('.dzlab_menu').animate({'left':'-100%'},300);
            $('.menu_bg').animate({'opacity':0},300,'linear',function(){
                $('.menu_bg').css({display:'none'});
            });
        });
        
        $('.dzlab_header span').click(function(){
            $('.fmenu').animate({'right':0},300);
            $('.fmenu_bg').css({display:'block'});
            $('.fmenu_bg').animate({'opacity':0.5},300);
        });
        var hideFMenu = function(){
            $('.fmenu').animate({'right':'-100%'},300);
            $('.fmenu_bg').animate({'opacity':0},300,'linear',function(){
                $('.fmenu_bg').css({display:'none'});
            });
        };
        $('.fmenu_bg').click(hideFMenu)
        $('.fmenu .dialog').click(hideFMenu)
        $('.catlist li').click(function(){
                var forumListIndex = $(this).index();
                $('.catlist li').removeClass('active');
                $(this).addClass('active');
                $('.forumlist ul').css({display:'none'});
                $('.forumlist ul:eq('+forumListIndex+')').css({display:'block'});
            })
        
        $('.tab_btn').click(function() {
            $('.tab_btn').removeClass('on');
            $(this).addClass('on');
        });

        if ($('.dzlab_jzloading').length > 0) {
            var body = $(document.body);
            PullToRefresh.init({
                mainElement: '.dzlab_jzloading',
                instructionsPullToRefresh: '向下拉动刷新',
                instructionsReleaseToRefresh: '松开立即刷新',
                instructionsRefreshing: '正在刷新',
                iconArrow: '<img src="/static/image/mobile/images/icon_load.gif" />',
                iconRefreshing: '<img src="/static/image/mobile/images/icon_load.gif" />',
                distMax: ios ? 0 : 110,
                distThreshold: ios ? 45 : 90,
                distReload: 82,
                onRefresh: function() {
                    Turbolinks.visit(window.location.href, { action: 'replace' });
                },
                shouldPullToRefresh: function() {
                    return !body.hasClass('lg-on') && window.scrollY <= 0
                }
            });
        }

        lightGallery(document.body, { selector: '.gallery-img' });
    });
})();

function showLoading() {
    spinner.css({display:'block', opacity: 0});
    spinner.fadeTo(150, 0.6);
}

function hideLoading() {
    clearTimeout(loadingTimer);
    if (!spinner) return;
    spinner.css({display:'none'});
}

function showfilter(){
    $('.fmenu').animate({'right':0},300);
    $('.fmenu_bg').css({display:'block'});
    $('.fmenu_bg').animate({'opacity':0.5},300);
}

function smoothScroll(el, to, duration) {
    if (duration < 0) {
        return;
    }
    var difference = to - $(window).scrollTop();
    var perTick = difference / duration * 10;
    this.scrollToTimerCache = setTimeout(function() {
        if (!isNaN(parseInt(perTick, 10))) {
            window.scrollTo(0, $(window).scrollTop() + perTick);
            smoothScroll(el, to, duration - 10);
        }
    }.bind(this), 10);
}

function evalScriptInOrder(scripts) {
    function evalScript(elem, callback) {
        var data = elem.text || elem.textContent || elem.innerHTML || '';

        if (elem.type && elem.type !== 'text/javascript') {
            callback();
            return;
        }
        var script = document.createElement('script');
        var parentElem = elem.parentNode;
        
        script.type = 'text/javascript';
        script.async = false;
    
        if (elem.src !== '') {
            script.src = elem.src;
            script.onload = function() { elem.onload && elem.onload(...arguments); callback(); };
            parentElem && parentElem.replaceChild(script, elem);
        } else {
            script.textContent = data;
            parentElem && parentElem.replaceChild(script, elem);
            callback();
        }
    };

    var i = 0;
    function execute_script(i) {
        script = scripts[i];
        evalScript(scripts[i], function() {
            if (i < scripts.length-1) {
                execute_script(++i);
            }
        });
    }

    execute_script(i);
}

function replaceNotAllowedWords(node) {
    if (node.nodeType == 3) {
        node.data = node.data.replace(/Weibo|微博|huya|虎牙|bilibili|B站|公众号|微信|QQ|Q群|头条|抖音|加群|\d{9}/gi, '*已屏蔽*');
    }
    if (node.nodeType == 1 && node.nodeName != 'SCRIPT') {
        for (var i = 0; i < node.childNodes.length; i++) {
            replaceNotAllowedWords(node.childNodes[i]);
        }
    }
}

function toggleMoreOptions() {
    var $moreOptions = $('.more_options');
    if ($moreOptions.css('display') === 'none') {
        showMoreOptions();
    } else {
        hideMoreOptions();
    }
}

function showMoreOptions() {
    var $moreOptions = $('.more_options');
    var $mask = $('#more_options_mask');
    $moreOptions.fadeIn(150);
    $mask.css({'display':'block','width':'100%','height':'100%','position':'fixed','top':'0','left':'0','background':'black','opacity':'0','z-index':'100'});
    $mask.fadeTo(150, 0.2);
}

function hideMoreOptions() {
    var $moreOptions = $('.more_options');
    var $mask = $('#more_options_mask');
    $moreOptions.fadeOut(150);
    $mask.animate({ opacity: 0 }, 150, 'linear', function() {
        $mask.css('display', 'none');
    });
}

function sharePage() {
    if (navigator.share) {
        navigator.share({
            text: document.title,
            url: window.location.href,
        }).then(function() {
            hideMoreOptions();
        });
    } else {
        popup.open('分享功能暂不支持你的浏览器', 'alert');
    }
}
