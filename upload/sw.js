importScripts(
  "https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js"
);
workbox.googleAnalytics.initialize();
workbox.routing.registerRoute(
  /source\/plugin\/tshuz_avatarcheck\/avatar\//,
  new workbox.strategies.NetworkOnly()
);
workbox.routing.registerRoute(
  /([\/|.|\w|\s|-])*\.(?:png|gif|jpg|jpeg|webp|svg|ico|ttf|woff|woff2|eot|otf|js|css)/,
  new workbox.strategies.CacheFirst({
    cacheName: "static-resources",
    plugins: [
      new workbox.expiration.Plugin({
        maxEntries: 500,
        maxAgeSeconds: 7 * 24 * 60 * 60, // 7 Days
        purgeOnQuotaError: true,
      }),
    ],
  })
);
workbox.routing.registerRoute(
  ({ event }) =>
    event.request.headers.has("X-Turbolinks") ||
    event.request.destination === "document",
  new workbox.strategies.NetworkFirst({
    networkTimeoutSeconds: 5,
    cacheName: "pages",
    plugins: [
      new workbox.expiration.Plugin({
        maxEntries: 100,
        maxAgeSeconds: 7 * 24 * 60 * 60, // 7 Days
        purgeOnQuotaError: true,
      }),
    ],
  })
);
workbox.routing.registerRoute(
  /https?:\/\/blob\.keylol\.com\//,
  new workbox.strategies.CacheFirst({
    cacheName: "blob",
    plugins: [
      new workbox.expiration.Plugin({
        maxEntries: 30,
        maxAgeSeconds: 1 * 24 * 60 * 60, // 1 Days
        purgeOnQuotaError: true,
      }),
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200],
      }),
    ],
  })
);
workbox.routing.registerRoute(
  /https?:\/\/([\/|.|\w|\s|-])*\.(?:ttf|woff|woff2|eot|otf|js|css)/,
  new workbox.strategies.StaleWhileRevalidate({
    cacheName: "external",
    plugins: [
      new workbox.expiration.Plugin({
        maxEntries: 30,
        maxAgeSeconds: 3 * 24 * 60 * 60, // 3 Days
        purgeOnQuotaError: true,
      }),
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200],
      }),
    ],
  })
);
