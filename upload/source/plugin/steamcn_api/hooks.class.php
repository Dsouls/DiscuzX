<?php

class plugin_steamcn_api {
	function __construct () {
		global $_G;
		$this->settings = $_G['cache']['plugin']['steamcn_api'];
	}
	public function common() {
		global $_G;
		if (!$this->settings['api_key']) return;
		if (STYLEID == $this->settings['style_id']) {
			if ($this->settings['api_key'] != $_SERVER['HTTP_X_API_KEY']) {
				die('Access denied!');
			} else {
				define('IN_API', true);
				$_GET['formhash'] = FORMHASH;
				header('content-type: application/json');
			}
		}
    }
}
