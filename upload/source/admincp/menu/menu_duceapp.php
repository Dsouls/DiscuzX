<?php

/**
 * Copyright (c) 2011 by duceapp.cn
 * Author: Hoolan Chan
 * Created: 2020-09-10
 * Version: 3.01222
 * Date: 2021-01-06 15:02:01
 * File: menu_duceapp.php
 * PHP library for duceapp - Support
 * - Documentation and latest version
 *       http://www.duceapp.cn/
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

global $admincp;

$duceapp_menukey = diconv('火狼', 'UTF-8', CHARSET);
$duceapp_plugkey = diconv('【火狼】', 'UTF-8', CHARSET);
$duceapp_base = diconv('【火狼】基础插件', 'UTF-8', CHARSET);
$duceapp_action = '';

loadcache('adminmenu');
foreach ($_G['cache']['adminmenu'] as $k => $v) {
    if (strpos($v['name'], $duceapp_plugkey) !== FALSE || strpos($v['name'], 'duceapp_') !== FALSE) {
        unset($_G['cache']['adminmenu'][$k]);
		if (!$admincp->perms['all'] && !$admincp->perms[$v['action']]) {
			continue;
		}
		if ($v['name'] == $duceapp_base && $menu[$duceapp_menukey]) {
			array_unshift($menu[$duceapp_menukey], array($v['name'], $v['action']));
		} else {
        	$menu[$duceapp_menukey][] = array($v['name'], $v['action']);
		}
		$duceapp_action = $v['action'];
    }
}

if (!empty($menu[$duceapp_menukey])) {
	array_unshift($menu[$duceapp_menukey], array('', $menuUrl = $duceapp_action.'&duceapp=addon'));
	$admincp->perms[$menuUrl] = true;
	$_topmenu = array();
	foreach ($topmenu as $_k => $_v) {
		$_topmenu[$_k] = $_v;
		if ($_k == 'plugin') {
			$_topmenu[$duceapp_menukey] = '';
		}
	}
	$topmenu = $_topmenu;
}