<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: ip_ipdb.php 5778 2020-04-28 12:00:00Z opensource $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

require_once DISCUZ_ROOT.'/source/class/lib/ipdb-php/vendor/autoload.php';

class ip_ipdb {
	
	private static $instance = NULL;
	private $ipdb = NULL;

	private function __construct() {
		$ipdatafile = constant("DISCUZ_ROOT").'./data/ipdata/ipdb.dat';
		$this->$ipdb = new ipip\db\City($ipdatafile);
	}

	public static function getInstance() {
		if (!self::$instance) {
			try {
				self::$instance = new ip_ipdb();
			} catch (Exception $e) {
				return NULL;
			}
		}
		return self::$instance;
	}

	public function convert($ip) {
		return diconv(' '.join(' ', $this->$ipdb->find($ip, 'CN')), 'UTF-8');
	}

}
?>