<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: lang_template.php 33692 2013-08-02 10:26:20Z nemohou $
 *
 *      This file is automatically generate
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

$lang = array (
  'getpassword' => '找回密码',
  'login_guest' => '没有帐号？<a href="member.php?mod={$_G[setting][regname]}">{$_G[setting][reglinkname]}</a>',
  'new_password' => '新密码',
  'new_password_confirm' => '确认密码',
  'password_weak' => '密码太弱，密码中必须包含',
  'strongpw_1' => '数字',
  'strongpw_2' => '小写字母',
  'strongpw_3' => '大写字母',
  'strongpw_4' => '特殊符号',
  'submit' => '提交',
  'close' => '关闭',
  'faq' => '帮助',
  'login' => '登录',
  'login_clearcookies' => '清除痕迹',
  'login_guestmessage' => '您需要先登录才能继续本操作',
  'login_seccheck2' => '请输入验证码后继续登录',
  'login_member' => '用户登录',
  'login_method' => '快捷登录',
  'login_permanent' => '自动登录',
  'profile_renew' => '请补充下面的登录信息',
  'register_from' => '推荐人',
  'account' => '帐号',
  'password' => '密码',
  'activation' => '激活',
  'agree' => '同意',
  'disagree' => '不同意',
  'index_activation' => '您的帐号需要激活',
  'invite_code' => '邀请码',
  'login_inactive' => '放弃激活，现在<a href="member.php?mod={$_G[setting][regname]}">{$_G[setting][reglinkname]}</a>',
  'login_now' => '已有帐号？现在登录',
  'password_confirm' => '确认密码',
  'register_buyinvitecode' => '还没有邀请码？点击此处获取',
  'register_email_tips' => '请输入正确的邮箱地址',
  'register_message' => '注册原因',
  'register_message1' => '您填写的注册原因会被当作申请注册的重要参考依据，请认真填写。',
  'register_password_length_tips1' => '最小长度为',
  'register_password_length_tips2' => '个字符',
  'register_password_tips' => '请填写密码',
  'register_repassword_tips' => '请再次输入密码',
  'register_username_tips' => '用户名（昵称）由 3 到 15 个字符组成',
  'register_validate_email_tips' => '注册后需要需要验证邮箱。<br />验证邮件有一定延迟，并有可能被归入你的垃圾邮件箱。',
  'rulemessage' => '使用条款',
);

?>