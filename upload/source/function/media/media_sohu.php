<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

$checkurl = array('my.tv.sohu.com/u/', 'v.blog.sohu.com/u/');

function media_sohu($url, $width, $height) {
	global $_G;
	if(preg_match("/^https?:\/\/my.tv.sohu.com\/(us?|pl)\/[^\/]+\/(\d+)/i", $url, $matches)) {
		$flv = $_G['scheme'].'://share.vrs.sohu.com/my/v.swf&topBar=1&id='.$matches[2].'&autoplay=false&from=page';
		if(!$width && !$height) {
			$api = 'http://v.blog.sohu.com/videinfo.jhtml?m=view&id='.$matches[2].'&outType=3';
			$str = file_get_contents($api, false, $ctx);
			if(!empty($str) && preg_match("/\"cutCoverURL\":\"(.+?)\"/i", $str, $image)) {
				$imgurl = str_replace(array('\u003a', '\u002e'), array(':', '.'), $image[1]);
			}
		}
	} elseif(preg_match("/^http:\/\/v.blog.sohu.com\/u\/[^\/]+\/(\d+)/i", $url, $matches)) {
		$flv = $_G['scheme'].'://share.vrs.sohu.com/my/v.swf&topBar=1&id='.$matches[1].'&autoplay=false&from=page';
		if(!$width && !$height) {
			$api = 'http://v.blog.sohu.com/videinfo.jhtml?m=view&id='.$matches[1].'&outType=3';
			$str = file_get_contents($api, false, $ctx);
			if(!empty($str) && preg_match("/\"cutCoverURL\":\"(.+?)\"/i", $str, $image)) {
				$imgurl = str_replace(array('\u003a', '\u002e'), array(':', '.'), $image[1]);
			}
		}
	}
	return array($flv, $iframe, $url, $imgurl);
}