<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: javascript.php 25246 2011-11-02 03:34:53Z zhangguosheng $
 */

header('Expires: '.gmdate('D, d M Y H:i:s', time() + 60).' GMT');

if(!defined('IN_API')) {
	exit('document.write(\'Access Denied\')');
}

loadcore();

include_once libfile('function/block');

loadcache('blockclass');
$bid = intval($_GET['bid']);
block_get_batch($bid);

$in_api = false;
if ($_SERVER['HTTP_X_API_KEY']) {
	loadcache('plugin');
	$in_api = $_SERVER['HTTP_X_API_KEY'] == $_G['cache']['plugin']['steamcn_api']['api_key'];
}

$data = block_fetch_content($bid, true, false, $in_api);

if ($in_api) {
	header('Content-Type: application/json');
	echo $data;
} else {
	$search = "/(href|src)\=(\"|')(?![fhtps]+\:)(.*?)\\2/i";
	$replace = "\\1=\\2$_G[siteurl]\\3\\2";
	$data = preg_replace($search, $replace, $data);
	
    dheader('Content-Type: application/javascript');
    
	echo 'document.write(\''.preg_replace("/\r\n|\n|\r/", '\n', addcslashes($data, "'\\")).'\');';
}

?>
